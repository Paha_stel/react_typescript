import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    background: string;
    borderRadius: string;
    table: {
      width: string;
    }
  }
}
