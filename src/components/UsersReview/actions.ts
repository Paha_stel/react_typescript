import { UserState } from './reducer';

import { GET_USERS, GET_USER } from './types';

export const getUsersAction = (users: UserState[]) => ({
  type: GET_USERS,
  payload: users,
});

export const getUserAction = (users: UserState) => ({
  type: GET_USER,
  payload: users,
});
