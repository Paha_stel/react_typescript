import { UserState } from './reducer';

export const GET_USERS = 'GET_USERS';
export const GET_USER = 'GET_USER';

interface GetUsersAction {
  readonly type: typeof GET_USERS;
  payload: UserState[];
}

interface GetUserAction {
  readonly type: typeof GET_USER;
  payload: UserState;
}

export interface GetUsersResponse {
  data: UserState[];
  page: number;
  per_page: number;
  total_pages: number;
}

export interface GetUserResponse {
  data: UserState;
}

export type GetUsers = GetUsersAction;

export type GetUser = GetUserAction

export type Action = GetUsers | GetUser;
