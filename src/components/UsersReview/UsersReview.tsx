import React, { FC, useEffect } from 'react';
import { connect } from 'react-redux';
import { UserTable } from 'components/UserTable';
import { AppState } from 'store';
import { Pagination } from 'components/Common/Pagination';
import { show as showAction } from 'redux-modal';
import { Modal } from 'components/Common/Modal';

import {
  getUsers as getUsersAction,
  getUserById as getUserByIdAction,
} from './epicActions';
import { UserState } from './reducer';
import { MODAL_NAME } from './constants';
import { UsersReviewContainer } from './UsersReview.style';

interface UsersReviewProps {
  getUsers: () => void;
  getUserById: (id: number) => Promise<UserState>;
  users: UserState[] | null;
  show: (name: string, option: Object) => void;
}

const connector = connect(
  ({ usersReview }: AppState) => ({
    users: usersReview.users,
  }),
  {
    getUsers: getUsersAction,
    getUserById: getUserByIdAction,
    show: showAction,
  },
);

const UsersReview: FC<UsersReviewProps> = ({
  getUsers,
  users,
  getUserById,
  show,
}) => {
  useEffect(() => {
    if (!users) {
      getUsers();
    }
  });

  return (
    <UsersReviewContainer>
      <Pagination update={getUsers}>
        <UserTable
          users={users || []}
          onClick={async (id: number) => {
            const user = await getUserById(id);
            show(MODAL_NAME, { user });
          }}
        />
      </Pagination>
      <Modal name={MODAL_NAME} />
    </UsersReviewContainer>
  );
};

export const ConnectedUsersReview = connector(UsersReview);
