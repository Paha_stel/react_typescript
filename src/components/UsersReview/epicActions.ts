import { Dispatch, Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { AppState } from 'store';
import { setPaginationAction } from 'components/Common/Pagination/actions';

import { getUsersAction, getUserAction } from './actions';
import { GetUsersResponse, GetUserResponse } from './types';
import { UserState } from './reducer';

export const getUsers = (): ThunkAction<Promise<void>, AppState, null, Action> => (
  (dispatch: Dispatch, getStore: () => AppState): Promise<void> => {
    const { pagination: { page, per_page } } = getStore();

    return fetch(`https://reqres.in/api/users?page=${page}&per_page=${per_page}`)
      .then((res: Response) => res.json())
      .then((res: GetUsersResponse) => {
        dispatch(getUsersAction(res.data));
        dispatch(setPaginationAction(res));
      });
  }
);

export const getUserById = (id: number): ThunkAction<Promise<UserState>, AppState, null, Action> => (
  (dispatch: Dispatch): Promise<UserState> => fetch(`https://reqres.in/api/users/${id}`)
    .then((res: Response) => res.json())
    .then((res: GetUserResponse) => {
      dispatch(getUserAction(res.data));

      return res.data;
    })
);
