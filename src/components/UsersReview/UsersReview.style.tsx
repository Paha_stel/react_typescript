import styled, { DefaultTheme } from 'styled-components';

interface UsersReviewProps {
  theme: DefaultTheme;
}

export const UsersReviewContainer = styled.div<UsersReviewProps>`
  width: ${({ theme }: UsersReviewProps) => theme.table.width};
`;
