import {
  Action,
  GET_USERS,
  GET_USER,
} from './types';

export interface UserState {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export interface UsersReviewState {
  users: UserState[] | null;
  currentUser: UserState | null;
}

const initialState: UsersReviewState = {
  users: null,
  currentUser: null,
};

export const usersReviewReducer = (state: UsersReviewState = initialState, action: Action): UsersReviewState => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
      };
    case GET_USER:
      return {
        ...state,
        currentUser: action.payload,
      };
    default:
      return state;
  }
};
