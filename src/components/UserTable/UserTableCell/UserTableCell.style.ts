import styled, { DefaultTheme } from 'styled-components';

interface ImgProps {
  theme: DefaultTheme;
}

export const UserTableCellContainer = styled.div`
  background-color: rgba(207, 206, 208, 0.2);
  box-shadow: 2px 2px 7px rgba(0,0,0,0.5);
  width: 200px;
  height: 300px;
  display: inherit;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  margin: 10px;
  cursor: pointer;
`;

export const Img = styled.img<ImgProps>`
  height: 100px;
  width: 100px;
  border-radius: ${({ theme }: ImgProps) => theme.borderRadius};
  margin-top: 10px;
`;

export const Text = styled.span`
  font-family: Arial, Helvetica, sans-serif;
  font-size: 1.2rem;
  font-weight: bold;
`;

export const InfoSection = styled.div`
  height: 120px;
  display: inherit;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;
