import React, { FC } from 'react';
import { UserState } from 'components/UsersReview/reducer';

import {
  UserTableCellContainer,
  InfoSection,
  Text,
  Img,
} from './UserTableCell.style';

interface UserTableCellProps {
  user: UserState;
  onClick: (id: number) => void;
}

export const UserTableCell: FC<UserTableCellProps> = ({ user, onClick }) => (
  <UserTableCellContainer key={user.id} onClick={() => { onClick(user.id); }}>
    <Img src={user.avatar}/>
    <InfoSection>
      <Text>
        {user.first_name}
      </Text>
      <Text>
        {user.last_name}
      </Text>
    </InfoSection>
  </UserTableCellContainer>
);
