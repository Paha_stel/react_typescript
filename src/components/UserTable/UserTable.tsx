import React, { Component } from 'react';
import { UserState } from 'components/UsersReview/reducer';

import { UserTableCell } from './UserTableCell';

import { TableContainer } from './UserTable.style';

interface TableProps {
  users: UserState[];
  onClick: (id: number) => void;
}

export class UserTable extends Component<TableProps> {

  render() {
    const { users, onClick } = this.props;

    return (
      <TableContainer>
        {users.map((user: UserState) => (
          <UserTableCell user={user} onClick={onClick}/>
        ))}
      </TableContainer>
    );
  }
}
