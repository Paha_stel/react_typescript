import styled, { DefaultTheme } from 'styled-components';

interface AppContainerProps {
  theme: DefaultTheme;
}

export const AppContainer = styled.div<AppContainerProps>`
  background: ${({ theme }: AppContainerProps) => theme.background};
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0;
  margin: 0;
`;
