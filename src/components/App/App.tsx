import React, { FC } from 'react';
import { UsersReview } from 'components/UsersReview';

import { AppContainer } from './App.style';

export const App: FC = () => (
  <AppContainer>
    <UsersReview />
  </AppContainer>
);
