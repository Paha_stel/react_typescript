import styled, { DefaultTheme } from 'styled-components';

interface ImgProps {
  theme: DefaultTheme;
}

export const ModalContainer = styled.div`
  width: 450px;
  height: 300px;
  position: fixed;
  left: calc(50% - 225px);
  top: calc(50vh - 150px);
  transition: background-color 100ms linear;
  /* background-color: rgba(207, 206, 208, 0.7); */
  background-color: rgba(207, 211, 223, 0.7);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;

  &:hover {
    /* background-color: rgba(207, 206, 208, 1); */
    background-color: rgba(207, 211, 223, 1);
  }
`;

export const Img = styled.img<ImgProps>`
  height: 100px;
  width: 100px;
  border-radius: ${({ theme }: ImgProps) => theme.borderRadius};
  margin-top: 10px;
`;

export const Button = styled.button`
  text-transform: uppercase;
  background: transparent;
  border: 1px solid black;
  width: 80px;
  height: 40px;
  font-size: 1.3rem;
  transition: all 100ms linear;

  &:hover {
    width: 100px;
    border: 1px solid rgb(38, 46, 205);
  }
`;

export const Info = styled.div`
  height: 120px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;

export const Text = styled.span`
  font-family: Arial, Helvetica, sans-serif;
  font-size: 1.2rem;
  font-weight: bold;
`;
