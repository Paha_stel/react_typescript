import React, { FC } from 'react';
import { connectModal, InjectedProps } from 'redux-modal';
import { UserState } from 'components/UsersReview/reducer';

import {
  ModalContainer,
  Button,
  Info,
  Text,
  Img,
} from './Modal.style';

interface ModalProps {
  name: string;
}

interface WithModalProps extends InjectedProps {
  user: UserState;
}

export const Modal: FC<ModalProps> = (props) => {
  const withModal: FC<WithModalProps> = ({ handleHide, user }) => (
    <ModalContainer>
      <Img src={user.avatar}/>
      <Info>
        <Text>
          {user.email}
        </Text>
        <Text>
          {user.first_name}
        </Text>
        <Text>
          {user.last_name}
        </Text>
      </Info>
      <Button onClick={handleHide}>ok</Button>
    </ModalContainer>
  );

  const ConnectedModal = connectModal(props)(withModal);

  return <ConnectedModal />;
};
