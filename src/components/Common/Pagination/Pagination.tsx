import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { AppState } from 'store';

import { setPageAction } from './actions';
import { PaginationContainer, Arrow } from './Pagination.style';

const connector = connect(
  ({ pagination }: AppState) => ({
    page: pagination.page,
    total_pages: pagination.total_pages,
  }),
  {
    setPage: setPageAction,
  },
);

interface PaginationProps {
  page: number;
  total_pages: number;
  setPage: (page: number) => void;
  update: () => void;
}

class Pagination extends Component<PaginationProps> {
  private readonly handleClickPrev = () => {
    const { page, setPage, update } = this.props;
    setPage(page - 1);
    update();
  }

  private readonly handleClickNext = () => {
    const { page, setPage, update } = this.props;
    setPage(page + 1);
    update();
  }

  public render() {
    const {
      children,
      page,
      total_pages,
    } = this.props;
    const hasPrev: boolean = Boolean(page - 1);
    const hasNext: boolean = Boolean(total_pages - page);

    return (
      <Fragment>
        {children}
        <PaginationContainer hasPrev={hasPrev}>
          {hasPrev && (
            <Arrow onClick={this.handleClickPrev}>
              ❮
            </Arrow>
          )}
          {hasNext && (
            <Arrow onClick={this.handleClickNext}>
              ❯
            </Arrow>
          )}
        </PaginationContainer>
      </Fragment>
    );
  }
}

export const ConnectedPagination = connector(Pagination);
