import { PaginationState } from './reducer';

export const SET_PAGE = 'SET_PAGE';
export const SET_PAGINATION = 'SET_PAGINATION';

export interface SetPageAction {
  readonly type: typeof SET_PAGE;
  payload: {
    page: number;
  };
}

export interface SetPaginationAction {
  readonly type: typeof SET_PAGINATION;
  payload: PaginationState;
}

export type Pagination = SetPageAction | SetPaginationAction;

export type Action = Pagination;
