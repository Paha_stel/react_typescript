import styled from 'styled-components';

interface ArrowProps {
  hasPrev: boolean;
}

export const PaginationContainer = styled.div<ArrowProps>`
  display: flex;
  justify-content: ${({ hasPrev }: ArrowProps) => hasPrev ? 'space-between' : 'flex-end'};
`;

export const Arrow = styled.a`
  cursor: pointer;
  width: 20px;
  height: 20px;
  &:hover {
    background-color: rgb(220, 220, 220);
  }
  text-align: center;
`;
