import {
  SET_PAGE,
  SET_PAGINATION,
  SetPageAction,
  SetPaginationAction,
} from './types';
import { PaginationState } from './reducer';

export const setPageAction = (page: number): SetPageAction => ({
  type: SET_PAGE,
  payload: { page },
});

export const setPaginationAction = (pagination: PaginationState): SetPaginationAction => ({
  type: SET_PAGINATION,
  payload: pagination,
});
