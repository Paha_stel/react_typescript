import {
  Action,
  SET_PAGE,
  SET_PAGINATION,
} from './types';

export interface PaginationState {
  page: number;
  per_page: number;
  total_pages: number;
}

const initialState: PaginationState = {
  page: 1,
  per_page: 4,
  total_pages: 1,
};

export const paginationReducer = (state: PaginationState = initialState, action: Action): PaginationState => {
  switch (action.type) {
    case SET_PAGE:
      return {
        ...state,
        page: action.payload.page,
      };
    case SET_PAGINATION:
      const {
        page,
        per_page,
        total_pages,
      } = action.payload;

      return {
        ...state,
        page,
        per_page,
        total_pages,
      };
    default:
      return state;
  }
};
