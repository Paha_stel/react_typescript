import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { usersReviewReducer } from 'components/UsersReview/reducer';
import { paginationReducer } from 'components/Common/Pagination/reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducer as modal } from 'redux-modal';

const rootReducer = combineReducers({
  usersReview: usersReviewReducer,
  pagination: paginationReducer,
  modal,
});

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore(
  rootReducer,
  {},
  composeWithDevTools(applyMiddleware(thunk)),
);
