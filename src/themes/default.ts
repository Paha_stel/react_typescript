import { DefaultTheme } from 'styled-components';

export const defaultTheme: DefaultTheme = {
  background: 'rgb(255, 255, 255)',
  borderRadius: '50%',
  table: {
    width: '50%',
  },
};
